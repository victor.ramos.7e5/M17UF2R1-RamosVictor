# M17UF2R1-RamosVictor

#  Estética: El juego

| Field             | Content                                                         |
|-------------------|-----------------------------------------------------------------|
| Developer         | Victor Ramos                                             |
| Genre             | Roguelike Shooter                                   |
| User Skills       | Disparar, cambiar de arma, hacer un dash                                   |
| Theme             | Fantasía                                                        |
| User Enjoyment    | Obtener las maximas monedas al eliminar a os enemigos           |
| Style             | Pixel Art                                                       |
| Sequentiality     | Tiempo real                                                     |
| Number of Players | Single Player          
| Dispositivos      | PC                                                              |
| Number of Players | Single Player      
| Tecnologia  des.  | Unity3D(C#)

## Controles:

###### MOVERSE
> WASD
> Flechas

###### ATACAR
> Click izquierdo
> Click Derecho Lanzar Granada

###### DASH
> Espacio

###### CAMBIO DE ARMA
> X / C

