using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    public Buttons gameover;
   /* public int PuntosTotales { get { return monedasTotales; } }
    private int monedasTotales;*/
    public PanelTiempo tiempo;
    public int min, seg;

    public bool AllEnemiesDown { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.Log("Hay mas de un Game Manager en la escena");
        }
    }
    /* public void SumarDinero(int puntosASumar)
    {
        monedasTotales += puntosASumar;
        Debug.Log(monedasTotales);
    }*/

    public void Morir()
    {
         gameover.Setup();
        tiempo.Desactivar();
    }

    
}
