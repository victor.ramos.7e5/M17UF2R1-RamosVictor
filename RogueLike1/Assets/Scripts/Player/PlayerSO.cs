using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayerSO : ScriptableObject
{
    public float speed;
    public float vida;
    public float dashcooldwon;
    public float longdash;
    public float dashspeed;
}
