using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Player : MonoBehaviour
{
    public static Player Instance { get; private set; }

    [SerializeField] public PlayerSO dataplayer;
    private Vector2 moveInput;
    private new Rigidbody2D rigidbody;

    public Transform target;

    public Image barravida;
    private float maxvida;
    public float vida;
    public float speed;


    private float activeMoveSpeed;
    private float dashspeed;
    private float dashLenfht = .5f, cooldown = 2f;

    private float dashCounter;
    private float dashCoolCounter;

    private Animator playerAnim;
    public GameObject grenade;
    public float numflashbang;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.Log("Hay mas de un Game Manager en la escena");
        }
    }
    void Start()
    {
        vida = dataplayer.vida;
        dashspeed = dataplayer.dashspeed;
        cooldown = dataplayer.dashcooldwon;
        dashLenfht = dataplayer.longdash;
        //canShoot = true;
        rigidbody = GetComponent<Rigidbody2D>();
        activeMoveSpeed = speed;
        maxvida = vida;
        playerAnim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        barravida.fillAmount = Mathf.Clamp(vida / maxvida, 0, 1);

        MoverPersonaje();
        LanzarFlash();
 
    }
    public void SumarGranada(float sumargranada)
    {
        numflashbang+=sumargranada;
    }

    void LanzarFlash()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (numflashbang > 0)
            {
                Instantiate(grenade, transform.position, Quaternion.identity);
                numflashbang--;
            }
        }
    }


    void MoverPersonaje()
    {
        
            /*float inputmovHori = Input.GetAxis("Horizontal");
            float inputmovVert = Input.GetAxis("Vertical");

            rigidbody.velocity = new Vector2(inputmovHori * speed, inputmovVert * speed);
            */

            moveInput.x = Input.GetAxis("Horizontal");
            moveInput.y = Input.GetAxis("Vertical");

            moveInput.Normalize();
            playerAnim.SetFloat("Horizontal", moveInput.x);
            playerAnim.SetFloat("Vertical", moveInput.y);
            playerAnim.SetFloat("Speed", moveInput.sqrMagnitude);
            rigidbody.velocity = moveInput * activeMoveSpeed;
            dash();
    }
    /*void MirarRaton()
    {
        Vector2 positionmouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.up = positionmouse - new Vector2(transform.position.x, transform.position.y);
    }*/


    public void RecibirDamage(float damage)
    {
        vida -= damage;

        if (vida <= 0)
        {
            SceneManager.LoadScene("GameOver");
            // GameManager.Instance.Morir();
        }
    }
    public void SumarVida(float masvida)
    {
        vida += masvida;

    }
    void dash()
    {
        if (Input.GetKeyDown(KeyCode.Space)) 
        {
            if (dashCoolCounter <= 0 && dashCounter<= 0)
            {
                activeMoveSpeed = dashspeed;
                dashCounter = dashLenfht;
            }
        }
        if (dashCounter > 0)
        {
            dashCounter -= Time.deltaTime;
            if (dashCounter <= 0)
            {
                activeMoveSpeed = speed;
                dashCoolCounter = cooldown;
            }
        }
        if (dashCoolCounter > 0)
        {
            dashCoolCounter -= Time.deltaTime;
        }
    }

    private IEnumerator ResetSpeed()
    {
        yield return new WaitForSeconds(5);
        activeMoveSpeed = speed;
    }

   public void AmentarVelocidad(float aumentovelocidad)
    {
        
            activeMoveSpeed = aumentovelocidad;
            StartCoroutine(ResetSpeed());
        
    }

}
