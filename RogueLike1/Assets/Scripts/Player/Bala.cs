using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{

    //public Weapons armas;
    public float damage;
    public float velocidad;

    private void Update()
    {
        transform.Translate(Vector2.up * velocidad * Time.deltaTime);

        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemigo"))
        {
            collision.GetComponent<Enemigo>().RecibirDamage(damage);
            Destroy(gameObject);
        }
        if (collision.tag == "Caja")
        {
            collision.GetComponent<ObjectoRompiblr>().RecibirDamage(damage);
            Destroy(gameObject);
        }
        else if (collision.CompareTag("Mapa"))
        {
            Destroy(gameObject);
        }
    }
}
