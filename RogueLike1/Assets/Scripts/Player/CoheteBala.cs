using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoheteBala : MonoBehaviour
{
    private float speed = 10f;
    private float damage = 100f;
    private new Animator animation;


    void Start()
    {
        animation = GetComponent<Animator>();
    }
    private void Update()
    {
        transform.Translate(Vector2.up * speed * Time.deltaTime);

        if (transform.position.y <= -10 || transform.position.y >= 10)
        {
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.CompareTag("Enemigo"))
        {
            collision.GetComponent<Enemigo>().RecibirDamage(damage);
            animation.SetTrigger("TriggerExplosion");
            speed = 0;
            Destroy(gameObject, 1.4f);
        }
        if (collision.CompareTag("Caja"))
        {
            collision.GetComponent<ObjectoRompiblr>().RecibirDamage(damage);
            animation.SetTrigger("TriggerExplosion");
            speed = 0;
            Destroy(gameObject, 1.4f);
        }
        else if (collision.CompareTag("Mapa"))
        {
            animation.SetTrigger("TriggerExplosion");
            speed = 0;
            Destroy(gameObject,1.4f);
        }

    }
    private void Explo()
    {
        Destroy(this.gameObject.GetComponent<Rigidbody2D>());
        Destroy(gameObject);
    }
}
