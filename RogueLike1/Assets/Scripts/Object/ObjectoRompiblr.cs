using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectoRompiblr : MonoBehaviour
{
    [SerializeField] float vida;
    [SerializeField] float probabilidadSpawn;
    [SerializeField] GameObject[] itemsSpawn;

   

    void Update()
    {
        
    }
    private void Destroy()
    {
        var spawnChance = Random.Range(0, 101);
        Debug.Log(spawnChance);

        if (spawnChance <= probabilidadSpawn)
        {
            var itemChance = Random.Range(0, itemsSpawn.Length);
            Instantiate(itemsSpawn[itemChance], this.transform.position, Quaternion.identity);
        }

        Destroy(this.gameObject);
    }
    public void RecibirDamage(float damage)
    {
        vida -= damage;

        if (vida <= 0)
        {
            Destroy(gameObject);
            Destroy();
        }
    }

}
