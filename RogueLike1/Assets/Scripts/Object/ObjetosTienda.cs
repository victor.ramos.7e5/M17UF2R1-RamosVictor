using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetosTienda : MonoBehaviour
{
    public enum Objetostienda
    {
        Potivida,
        PotiVelocidad,
        Saco,
        Cegadora
    };

    [SerializeField] Objetostienda objetostienda;

    public void UsoObjeto()
    {
        Player player = GameObject.FindObjectOfType<Player>();
        switch (objetostienda)
        {
            case Objetostienda.Potivida:
                player.SumarVida(10);
                break;
            case Objetostienda.PotiVelocidad:
                player.AmentarVelocidad(20f);
                break;
            case Objetostienda.Saco:
                var value = Random.Range(1, 30);
                HUD.Instance.SumarDinero(value);
                break;
            case Objetostienda.Cegadora:
                Player.Instance.SumarGranada(1);
                break;
            default:
                break;
        }
        Destroy(this.gameObject);
    }
}
