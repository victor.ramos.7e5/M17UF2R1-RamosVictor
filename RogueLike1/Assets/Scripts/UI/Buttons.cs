using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Buttons : MonoBehaviour
{
	public GameObject vida;
	public void Setup()
	{
		gameObject.SetActive(true);
		vida.SetActive(false);
	}
	public void Game1Button()
	{
		SceneManager.LoadScene("Game");
	}
	public void MenuButton()
	{
		SceneManager.LoadScene("Menu");
	}

}
