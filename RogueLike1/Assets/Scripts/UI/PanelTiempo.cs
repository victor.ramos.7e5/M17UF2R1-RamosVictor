using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PanelTiempo : MonoBehaviour
{
    //[SerializeField] int min, seg;
    [SerializeField] Text tiempo;

    private float restante;
    private bool enMarcha;



    private void Awake()
    {
        restante = (GameManager.Instance.min * 60) + GameManager.Instance.seg;
        enMarcha = true;

        
    }

    // Update is called once per frame
    void Update()
    {
        if (enMarcha)
        {
            restante -= Time.deltaTime;
            if (restante < 1)
            {
                enMarcha = false;

                SceneManager.LoadScene("GameOver");
            }
            int tempMin = Mathf.FloorToInt(restante / 60);
            int tempseg = Mathf.FloorToInt(restante % 60);
            tiempo.text = string.Format("{00:00}:{01:00}", tempMin, tempseg);
        }
    }
    public void Desactivar()
    {
        gameObject.SetActive(false);
        enMarcha = false;
    }
}
