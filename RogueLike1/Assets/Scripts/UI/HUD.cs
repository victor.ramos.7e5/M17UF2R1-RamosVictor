using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HUD : MonoBehaviour
{
    public static HUD Instance { get; private set; }
    
    public TextMeshProUGUI balas;
    public TextMeshProUGUI granadastext;
    public TextMeshProUGUI monedas;
    [SerializeField]
    private GameObject inventariopotis;

    private int precio;
    private int monedasactuales;
    private int objetosMaximos;
    [SerializeField] private GameObject tienda;

 

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.Log("Hay mas de un Game Manager en la escena");
        }
    }

    // Update is called once per frame
    void Update()
    {
        //monedasactuales = GameManager.Instance.PuntosTotales;
        balas.text = Weapons.Instance.cargador.ToString();

        monedas.text = monedasactuales.ToString();
        granadastext.text = Player.Instance.numflashbang.ToString();
        
    }
    public void Reanudar()
    {
        Time.timeScale = 1f;
        tienda.SetActive(false);
    }

    public void SumarDinero(int puntosASumar)
    {
        monedasactuales += puntosASumar;
        Debug.Log(monedasactuales);
    }

    #region TIENDA

    public void PrecioObjeto(string objeto)
    {
        switch (objeto)
        {
            case "PocionVelocidad":
                precio = 5;
                break;
            case "PocionVida":
                precio = 10;
                break;
            case "SacoMonedas":
                precio = 15;
                break;
            case "Cegadora":
                precio = 15;
                break;
        }
    }

    public void ConseguirObjeto(string objeto)
    {
        PrecioObjeto(objeto);

        if (precio <= monedasactuales && objetosMaximos < 3)
        {
            objetosMaximos++;
            monedasactuales -= precio;
            monedas.text = monedasactuales.ToString();

            GameObject equipo = (GameObject)Resources.Load(objeto);
            Instantiate(equipo, Vector3.zero, Quaternion.identity, inventariopotis.transform);

        }
    }
    #endregion
}
