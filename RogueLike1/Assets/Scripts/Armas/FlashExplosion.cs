using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashExplosion : MonoBehaviour
{
    private Vector2 center;
    public float radio;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Collider2D[] enemyHit = Physics2D.OverlapCircleAll(transform.position, radio);

        foreach (Collider2D colision in enemyHit)
        {
            Enemigo enemiesStun = colision.GetComponent<Enemigo>();
            if (enemiesStun != null)
            {
                enemiesStun.StunExplosion(5);
            }
        }
    }

    public void Destroyit()
    {
        Destroy(gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawSphere(transform.position, radio);
    }
}
