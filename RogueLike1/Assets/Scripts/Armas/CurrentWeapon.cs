using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentWeapon : MonoBehaviour
{
    int totalweapon;
    public int currentWeapon;

    public GameObject[] armas;
    public GameObject weaponHolder;
    public GameObject currentgun;

    void Start()
    {
        totalweapon = weaponHolder.transform.childCount;
        armas = new GameObject[totalweapon];

        for (int i = 0; i < totalweapon; i++)
        {
            armas[i] = weaponHolder.transform.GetChild(i).gameObject;
            armas[i].SetActive(false);
        }
        armas[0].SetActive(true);
        currentgun = armas[0];
        currentWeapon = 0;
    }
    void Update()
    {
        CambiarArmas();
    }

    public void CambiarArmas()
    {
        if (Input.GetKeyDown(KeyCode.C) && currentWeapon != 4)
        {
            //Siguiente arma
            armas[currentWeapon].SetActive(false);
            currentWeapon += 1;
            armas[currentWeapon].SetActive(true);
            currentgun = armas[currentWeapon];
        }
        if (Input.GetKeyDown(KeyCode.X) && currentWeapon != 0)
        {
            //Siguiente arma
            ActivarArmas(currentWeapon);
            armas[currentWeapon].SetActive(false);
            currentWeapon -= 1;
            armas[currentWeapon].SetActive(true);
            currentgun = armas[currentWeapon];
        }
    }
    public void ActivarArmas(int num)
    {
        for (int i = 0; i < armas.Length; i++)
        {
            armas[i].SetActive(false);
        }
        armas[num].SetActive(true);
    }
}
