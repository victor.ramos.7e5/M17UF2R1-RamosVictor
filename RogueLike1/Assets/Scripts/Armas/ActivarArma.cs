using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarArma : MonoBehaviour
{
    public CurrentWeapon recogerArma;
    public int numeroArma;
    void Start()
    {
        recogerArma = GameObject.FindGameObjectWithTag("Player").GetComponent<CurrentWeapon>();
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag =="Player")
        {
            recogerArma.ActivarArmas(numeroArma);
            Destroy(gameObject);
        }
    }
}
