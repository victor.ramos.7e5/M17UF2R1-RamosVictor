using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashBang : MonoBehaviour
{
    private Vector3 targetpos;

    public float speed = 5;
    public GameObject explosion;
    void Start()
    {
        targetpos = GameObject.Find("SaildaBala").transform.position;
    }

    
    void Update()
    {
        if (speed>0)
        {
            speed -= Random.Range(0.1f, 25f);
            transform.position = Vector2.MoveTowards(transform.position, targetpos, speed * Time.deltaTime);

        }
        else if (speed<0)
        {
            speed = 0;
            StartCoroutine(Explode(1));
        }
    }

    IEnumerator Explode(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
        Instantiate(explosion, transform.position, Quaternion.identity);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemigo")
        {
            StartCoroutine(Explode(1));
        }
    }
}
