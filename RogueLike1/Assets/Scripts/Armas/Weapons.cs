using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TipoArma
{
    sniper,
    pistola,
    rafagas,
    lanzacohetes,
    minigun
}
public class Weapons : MonoBehaviour
{
    public static Weapons Instance { get; private set; }
    public Transform contrlDisparo;
    public GameObject bala;
    public float cargador;
    //public float balastotales;
    public float damage;
    public float cadencia;
    private float velocidad;
    public float cooldown;
    public bool canshoot;


    public WeaponsSO datosArmas;
    public TipoArma arma;

    private int numrafaga;
    public int rafaga;




    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.Log("Hay mas de un Weapon en la escena");
        }
    }
    private void Start()
    {
        
        velocidad = datosArmas.velocidad;
        cargador = datosArmas.cargador;
        cadencia = datosArmas.cadencia;
        cooldown = datosArmas.cooldown;
        canshoot = true;
    }

    void Update()
    {
       /* Debug.Log(velocidad);
        Debug.Log(datosarmas.velocidad);*/

        MirarRaton();
        switch (arma)
        {
            case TipoArma.sniper:
                DisparoPotente();
                break;
            case TipoArma.pistola:
                Disparopistola();
                break;
            case TipoArma.rafagas:
                DisparoRafagas();
                break;
            case TipoArma.lanzacohetes:
                DisparoPotente();
                break;
            case TipoArma.minigun:
                DisparoRafagas();
                break;
            default:
                break;
        }
    }

    private void Disparopistola() 
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Disparar();
        }
    }

    private void DisparoPotente()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            if (canshoot)
            {
                if (Time.time > cooldown)
                {
                    cooldown = Time.time + cadencia;
                    Disparar();
                }
            }
        }
    }

    public void Disparar()
    {
        if (cargador > 0)
        {
            GameObject balaGO = Instantiate(bala, contrlDisparo.position, contrlDisparo.rotation);
            balaGO.GetComponent<Bala>().damage = datosArmas.damage;
            balaGO.GetComponent<Bala>().velocidad = datosArmas.velocidad;
            cargador--;

        }
    }
    public void MirarRaton()
    {
        Vector2 positionmouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.up = positionmouse - new Vector2(transform.position.x, transform.position.y);
    }

    private void DisparoRafagas()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            if (canshoot)
            {
                Disparar();
                StartCoroutine(ShootCooldown());
}
            
        }
    }
    private IEnumerator ShootCooldown(/*float t*/)
    {
        canshoot = false;
        yield return new WaitForSeconds(0.15f);
        numrafaga++;
        if (numrafaga >= rafaga)
        {
            yield return new WaitForSeconds(0.25f);
            numrafaga = 0;
        }
        canshoot = true;
    }
}
