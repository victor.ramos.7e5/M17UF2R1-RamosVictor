using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class WeaponsSO : ScriptableObject
{
    public float cargador;
    public float balasatotales;
    public float damage;
    public float cadencia;
    public float velocidad;
    public float cooldown;

}
