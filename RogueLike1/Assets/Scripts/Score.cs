using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    private float score;

    private TextMeshProUGUI scoreText;



    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        scoreText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        score += Time.deltaTime;
        scoreText.text = score.ToString("0");
    }

    public void SumarPuntos(float puntosextra)
    {
        score += puntosextra;
    }
}
