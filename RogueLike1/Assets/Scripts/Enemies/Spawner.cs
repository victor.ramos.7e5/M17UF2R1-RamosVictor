using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private GameObject[] enemies;
    [SerializeField]
    private float spawnTime = 1f;
    [SerializeField]
    private float limit_enemies = 10;
    private int enemy_counter = 0;
    private bool onCooldown;


    // Start is called before the first frame update
    void Start()
    {
        onCooldown = false;
        // StartCoroutine(spawnEnemies(spawntime, enemies));

    }

    private void Update()
    {
        int RandomNum = Random.Range(0, enemies.Length);

        if (onCooldown == false && enemy_counter != limit_enemies )
        {
            float newX = Random.Range(-10f, 10f);
            float newY = Random.Range(-5f, 5f);
            var instance1 = Instantiate(enemies[RandomNum], new Vector3(newX, newY, 0), Quaternion.identity);
            enemy_counter++;

            onCooldown = true;

            Invoke("Cooldown", spawnTime);


        }


    }

    private void Cooldown()
    {
        onCooldown = false;

    }
    

   
    /*
    private IEnumerator SpawnEnemies(/*float interval, GameObject[] enemy*)
    {
        /*
         yield return new WaitForSeconds(interval);
         GameObject newEnemy = Instantiate(enemy[RandomNum], new Vector3(Random.Range(-5f, 5f), Random.Range(-6f, 6f), 0), Quaternion.identity);
         StartCoroutine(spawnEnemies(interval, enemy));*

        int RandomNum = Random.Range(0, enemies.Length);
        Vector2 spawnpos = GameObject.Find("Player").transform.position;
        spawnpos += Random.insideUnitCircle.normalized * spawnradio;
        Instantiate(enemies[RandomNum], spawnpos, Quaternion.identity);
        yield return new WaitForSeconds(spawntime);
        StartCoroutine(SpawnEnemies());
    }*/
}
