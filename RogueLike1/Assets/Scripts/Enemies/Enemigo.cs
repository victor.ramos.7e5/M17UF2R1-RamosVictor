using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum Enemigos
{
    melee,
    distancia
}
public class Enemigo : Enemies
{
    [SerializeField] float probabilidadSpawn;
    [SerializeField] GameObject[] itemsSpawn;
  
    public Enemigos tipo;
    private Rigidbody2D rb;
    private Vector2 movement;
    public GameObject bala;
    public Transform punt_inst;
    private bool explosion;

    public float puntosesxtra;

    [SerializeField] private Score score;
   // private bool canmove = false;
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();

    }

    
    void Update()
    {
        Vector3 direction = Player.Instance.target.position- transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x)*Mathf.Rad2Deg;
        rb.rotation=angle;
        direction.Normalize();
        movement = direction;
    }
    private void FixedUpdate()
    {
        switch (tipo)
        {
            case Enemigos.melee:
                MovimientoEnemy(movement);
                break;
            case Enemigos.distancia:
               EnemieGun();
                break;
            default:
                break;
        }

       
    }
    void MovimientoEnemy(Vector2 direction)
    {
        rb.MovePosition((Vector2)transform.position + (direction * speed * Time.deltaTime));
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Player>().RecibirDamage(damage);
            Destroy(gameObject);
        }

    } 
    

  
    void EnemieGun()
    {

        tiempodisparo += Time.deltaTime;
        if (tiempodisparo > 2)
        {
            tiempodisparo = 0;
            shoot();
        }
     
    }

    void shoot() 
    {
       Instantiate(bala, punt_inst.position, Quaternion.identity);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.GetComponent<Player>().RecibirDamage(damage);
            Destroy(gameObject);
        }
        if (collision.CompareTag("Mapa")&& collision.CompareTag("Blastoise"))
        {
            Destroy(gameObject);
        }
    }


    public void RecibirDamage(float damage)
    {
        vida -= damage;

        if (vida <= 0)
        {
            score.SumarPuntos(puntosesxtra);
            SpawnCoins();
            Destroy(gameObject);
        }
    }

    public void StunExplosion(float tiempostun)
    {
        if (!explosion)
        {
            explosion = true;
           // speed = 0;
            tiempodisparo = -7;
            StartCoroutine(CoolDown(tiempostun));
        }
    }

    IEnumerator CoolDown(float tiempostun)
    {
        yield return new WaitForSeconds(tiempostun);
        speed = 0;
        explosion = false;
    }
    private void SpawnCoins()
    {
        var spawnChance = Random.Range(0, 101);
        Debug.Log(spawnChance);

        if (spawnChance <= probabilidadSpawn)
        {
            var itemChance = Random.Range(0, itemsSpawn.Length);
            Instantiate(itemsSpawn[itemChance], this.transform.position, Quaternion.identity);
        }

        Destroy(this.gameObject);
    }
}
