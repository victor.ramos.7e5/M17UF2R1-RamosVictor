using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rooms : MonoBehaviour
{
    public GameObject[] walls;
    public GameObject[] doors; // Derecha-0 Abajo-1 Izquierda-2 Arriba-3

    //[SerializeField]
   // private bool[] pruebaPuertas;


   /* private void Start()
    {
        UpdtaeRoom(pruebaPuertas);
    }*/
    public void UpdateRoom(bool[] status)
    {
        for (int i = 0; i < status.Length; i++)
        {
            doors[i].SetActive(status[i]);
            walls[i].SetActive(!status[i]);
        }
    }
}
